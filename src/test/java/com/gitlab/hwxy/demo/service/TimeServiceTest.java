package com.gitlab.hwxy.demo.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * test
 *
 * @author hwxy
 * @date 2020/03/01.
 */
@SpringBootTest
class TimeServiceTest {
    @Autowired
    private TimeService timeService;

    @Test
    public void testDate() {
        timeService.getDate(2020, 2, 29);
        timeService.getNextWorkDay();
        timeService.getNextNearFirstWorkDay();
        timeService.getTime(23, 59, 59);
        timeService.getRomeDateTime();
    }
}
