package com.gitlab.hwxy.demo.router;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.gitlab.hwxy.demo.handler.TimeHandler;

/**
 * t
 *
 * @author hwxy
 * @date 2020/02/29.
 */
@SpringBootTest
class TimeRouterTest {
    @Autowired
    private TimeHandler timeHandler;

    @Autowired
    private TimeRouter timeRouter;

    @Test
    void getTime() {
        WebTestClient webTestClient =
                WebTestClient.bindToRouterFunction(timeRouter.getTime(timeHandler)).build();

        webTestClient.get()
                .uri("/time")
                .exchange()
                .expectStatus()
                .isOk();
    }
}
