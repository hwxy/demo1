package com.gitlab.hwxy.demo.router;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.gitlab.hwxy.demo.handler.HelloHandler;

/**
 * test
 *
 * @author hwxy
 * @date 2020/02/29.
 */
@SpringBootTest
class HelloRouterTest {
    @Autowired
    HelloRouter helloRouter;

    @Autowired
    HelloHandler helloHandler;

    @Test
    void helloRoute() {
        WebTestClient client =
                WebTestClient.bindToRouterFunction(helloRouter.helloRoute()).build();

        client.get()
                .uri("/hello")
                .exchange()
                .expectStatus()
                .isOk();
    }
}
