package com.gitlab.hwxy.demo.handler;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

/**
 * handler
 *
 * @author hwxy
 * @date 2020/02/23.
 */
@Component
public class HelloHandler {
    /**
     * get请求测试
     *
     * @param request 请求
     * @return hw
     */
    public Mono<ServerResponse> handleHello(ServerRequest request) {
        return ServerResponse.ok().contentType(MediaType.TEXT_PLAIN)
                .bodyValue("Hello World!");
    }
}
