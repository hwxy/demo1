package com.gitlab.hwxy.demo.handler;

import java.time.Instant;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

/**
 * 测试Java8 time api
 *
 * @author hwxy
 * @date 2020/02/29.
 */
@Component
public class TimeHandler {
    /**
     * 返回当前时间戳
     * 
     * @param serverRequest re
     * @return 秒
     */
    public Mono<ServerResponse> getNowSeconds(ServerRequest serverRequest) {
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(Instant.now().getEpochSecond()), Long.class);
    }
}
