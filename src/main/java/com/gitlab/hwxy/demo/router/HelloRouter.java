package com.gitlab.hwxy.demo.router;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.gitlab.hwxy.demo.handler.HelloHandler;

/**
 * 路由
 *
 * @author hwxy
 * @date 2020/02/23.
 */
@Configuration
public class HelloRouter {
    @Autowired
    private HelloHandler helloHandler;

    /**
     * say hello
     * 
     * @return hw
     */
    @Bean
    public RouterFunction<ServerResponse> helloRoute() {
        return RouterFunctions.route(
                RequestPredicates.GET("/hello").and(RequestPredicates.accept(MediaType.TEXT_PLAIN)),
                helloHandler::handleHello);
    }
}
