package com.gitlab.hwxy.demo.router;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.gitlab.hwxy.demo.handler.TimeHandler;

/**
 * t
 *
 * @author hwxy
 * @date 2020/02/29.
 */
@Configuration
public class TimeRouter {

    /**
     * GET
     * route time
     * 
     * @param timeHandler th
     * @return th response
     */
    @Bean
    public RouterFunction<ServerResponse> getTime(TimeHandler timeHandler) {
        return RouterFunctions.route(RequestPredicates.GET("/time"), timeHandler::getNowSeconds)
                .andRoute(RequestPredicates.GET("/time/now"), timeHandler::getNowSeconds);
    }
}
