package com.gitlab.hwxy.demo.service;

import java.time.*;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

import org.springframework.stereotype.Service;

import com.gitlab.hwxy.demo.bean.DateBean;
import com.gitlab.hwxy.demo.bean.DateTimeBean;
import com.gitlab.hwxy.demo.bean.TimeBean;

/**
 * time service
 *
 * @author hwxy
 * @date 2020/03/01.
 */
@Service
public class TimeService {
    /**
     * 计算工作日
     */
    private TemporalAdjuster workDayAdjuster;
    /**
     * 罗马
     */
    private ZoneId romeZoneId;

    public TimeService() {
        workDayAdjuster = temporal -> {
            DayOfWeek dayOfWeek = DayOfWeek.of(temporal.get(ChronoField.DAY_OF_WEEK));
            int dayAdd = 0;
            switch (dayOfWeek) {
                case FRIDAY:
                    dayAdd = 3;
                    break;
                case SATURDAY:
                    dayAdd = 2;
                    break;
                default:
                    dayAdd = 1;
            }
            return temporal.plus(dayAdd, ChronoUnit.DAYS);
        };
        romeZoneId = ZoneId.of("Europe/Rome");
    }

    /**
     * 年月日构造ld
     * 
     * @param year 年
     * @param month 月
     * @param day 日
     * @return ld
     */
    public DateBean getDate(int year, int month, int day) {
        LocalDate localDate = LocalDate.of(year, month, day);
        return getDateBean(localDate);
    }

    /**
     * 获得下个工作日
     * 
     * @return db
     */
    public DateBean getNextWorkDay() {
        LocalDate localDate = LocalDate.now().with(workDayAdjuster);
        return getDateBean(localDate);
    }

    /**
     * 明年第一个工作日
     * 
     * @return db
     */
    public DateBean getNextNearFirstWorkDay() {
        LocalDate localDate =
                LocalDate.now().with(TemporalAdjusters.lastDayOfYear()).with(workDayAdjuster);
        return getDateBean(localDate);
    }

    private DateBean getDateBean(LocalDate localDate) {
        DateBean dateBean = DateBean.builder().year(localDate.getYear()).month(localDate.getMonth())
                .dayOfMonth(localDate.getDayOfMonth()).dayOfWeek(localDate.getDayOfWeek())
                .lenOfMonth(localDate.lengthOfMonth()).leap(localDate.isLeapYear()).build();
        System.out.println(dateBean);
        return dateBean;
    }

    /**
     * 获取时间
     * 
     * @param hour 时
     * @param min 分
     * @param second 秒
     * @return 时间
     */
    public TimeBean getTime(int hour, int min, int second) {
        LocalTime localTime = LocalTime.of(hour, min, second);
        return getTimeBean(localTime);
    }

    private TimeBean getTimeBean(LocalTime localTime) {
        TimeBean timeBean = TimeBean.builder().hour(localTime.getHour()).min(localTime.getMinute())
                .second(localTime.getSecond()).build();
        System.out.println(timeBean);
        return timeBean;
    }

    /**
     * 获取罗马时间
     * 
     * @return dtb
     */
    public DateTimeBean getRomeDateTime() {
        Instant now = Instant.now();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(now, romeZoneId);
        DateBean dateBean = getDateBean(localDateTime.toLocalDate());
        TimeBean timeBean = getTimeBean(localDateTime.toLocalTime());
        return getDateTimeBean(dateBean, timeBean);
    }

    private DateTimeBean getDateTimeBean(DateBean dateBean, TimeBean timeBean) {
        DateTimeBean dateTimeBean =
                DateTimeBean.builder().dateBean(dateBean).timeBean(timeBean).build();
        System.out.println(dateTimeBean);
        return dateTimeBean;
    }


}
