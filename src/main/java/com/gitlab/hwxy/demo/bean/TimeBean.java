package com.gitlab.hwxy.demo.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * 时间
 *
 * @author hwxy
 * @date 2020/03/01.
 */
@Data
@AllArgsConstructor
@Builder
public class TimeBean {
    private int hour;

    private int min;

    private int second;
}
