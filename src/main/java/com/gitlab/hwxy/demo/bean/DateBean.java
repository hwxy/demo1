package com.gitlab.hwxy.demo.bean;

import java.time.DayOfWeek;
import java.time.Month;

import lombok.Builder;
import lombok.Data;

/**
 * date bean
 *
 * @author hwxy
 * @date 2020/03/01.
 */
@Data
@Builder
public class DateBean {
    /**
     * 年
     */
    private int year;
    /**
     * 月
     */
    private Month month;
    /**
     * 日
     */
    private int dayOfMonth;
    /**
     * 星期几
     */
    private DayOfWeek dayOfWeek;
    /**
     * 这月的长度
     */
    private int lenOfMonth;
    /**
     * 闰年？
     */
    private boolean leap;
}
