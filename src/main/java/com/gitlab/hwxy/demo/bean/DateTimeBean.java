package com.gitlab.hwxy.demo.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * 日期时间
 *
 * @author hwxy
 * @date 2020/03/01.
 */
@Data
@AllArgsConstructor
@Builder
public class DateTimeBean {
    private DateBean dateBean;

    private TimeBean timeBean;
}
